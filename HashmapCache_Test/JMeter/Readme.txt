﻿ACHTUNG!
Vor der Verwendung des Loadtest unbedingt die Parameter unter Thread Group beachten.
Einstestellt ist per Default 100 Concurrent User machen 10.000 Requests bei einer Rampup-Time von 5 Sekunden.

Auf dem Zielsystem und dem Netzwerk erzeugt das einen ziemlich heftigen Load.

Beispiel:
Host: 24 Core Server XEON E5 ==> 60% Auslastung
Netzwerk: 1 Gigabit/s ==> ca. 60% Auslastung (600MBit Down)

Die Grafik verdeutlicht jedoch wie gut 1 Container die Last ab kann.
Gesamtlaufzeit des Test: 277 Sekunden.
Soll heißen in dieser Konstellation schafft ein Container >3600 Requests / Sekunde
