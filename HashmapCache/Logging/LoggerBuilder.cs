﻿using System;
using System.Runtime.CompilerServices;
using Serilog;
using Serilog.Core;
using Serilog.Enrichers.ShortTypeName;
using Serilog.Sinks.SystemConsole.Themes;

namespace HashmapCache.Logging;

public static class LoggerExtensions
{
    public static ILogger ExtendedContext(this ILogger logger,
        [CallerMemberName] string memberName = "",
        [CallerFilePath] string sourceFilePath = "",
        [CallerLineNumber] int sourceLineNumber = 0
    ) {
        return logger
            .ForContext("MemberName", memberName)
            .ForContext("FilePath", sourceFilePath)
            .ForContext("LineNumber", sourceLineNumber);
    }
}

public static class LoggerBuilder
{
    public static ILogger GetLogger<T>() => Log.ForContext<T>().ExtendedContext();
    public static ILogger GetLogger(Type type) => Log.ForContext(type).ExtendedContext();

}