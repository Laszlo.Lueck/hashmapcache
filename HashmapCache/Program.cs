using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using HashmapCache.Logging;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Enrichers.ShortTypeName;
using Serilog.Sinks.SystemConsole.Themes;

namespace HashmapCache;

public static class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    private static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration((_, config) => { })
            .UseSerilog((_, loggerConfiguration) =>
            {
                loggerConfiguration
                    .Enrich.WithShortTypeName()
                    .MinimumLevel.Information()
                    .WriteTo.Console(theme: AnsiConsoleTheme.Code,
                        outputTemplate:
                        "[{Timestamp:yyy-MM-dd HH:mm:ss} {Level:u3} {ShortTypeName}] {Message:lj}{NewLine}{Exception}");
            })
            .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
}