﻿using System.Threading.Tasks;
using HashmapCache.Configuration;
using HashmapCache.Tos;

namespace HashmapCache.Services;

public interface ICacheEntryResolver
{
    public Task<GetItemResponseObject> GetItem(string url, int ttl, ConfigurationItems configurationItems);
}