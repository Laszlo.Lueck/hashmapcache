﻿using Microsoft.Extensions.DependencyInjection;

namespace HashmapCache.Services;

public static class MemoryCacheFactory
{
    public static void BuildMemoryCache(this IServiceCollection services)
    {
        services.AddSingleton(new CustomMemoryCacheImpl());
    }
}