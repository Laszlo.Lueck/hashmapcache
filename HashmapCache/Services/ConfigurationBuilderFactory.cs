﻿using System;
using HashmapCache.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Optional.Unsafe;

namespace HashmapCache.Services;

public static class ConfigurationBuilderFactory
{
    public static void AddConfiguration(this IServiceCollection services)
    {
        var configurationOpt = new ConfigurationBuilder().GetConfiguration();
        if (!configurationOpt.HasValue)
        {
            Console.WriteLine("problems while reading environment variables, exit application");
            Environment.Exit(-1);
        }

        var configurationItems = configurationOpt.ValueOrFailure();
        services.AddSingleton(configurationItems);

    }
        
}