﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using HashmapCache.Logging;
using Optional;
using Serilog;

namespace HashmapCache.Services;

public static class SourceUpdater
{
    private static readonly ILogger Logger = LoggerBuilder.GetLogger(typeof(SourceUpdater));

    public static async Task<Option<MemoryStream>> DownloadAsStream(string url, string userAgent,
        int timeOut)
    {
        var cts = new CancellationTokenSource();
        var sw = Stopwatch.StartNew();
        try
        {
            cts.CancelAfter(timeOut);
            return await Task.Run(async () =>
            {
                var fileReq = new HttpClient();
                fileReq.DefaultRequestHeaders.Add("UserAgent", userAgent);
                    
                var returnStream = new MemoryStream();
                var stream = await fileReq.GetStreamAsync(url, cts.Token);

                await stream.CopyToAsync(returnStream, cts.Token);
                returnStream.Position = 0;
                return returnStream.Some();
            }, cts.Token);
        }
        catch (TaskCanceledException tke)
        {
            Logger.Error(tke, "the download takes to much time, cancelled");
            return Option.None<MemoryStream>();
        }
        catch (Exception exception)
        {
            Logger.Error(exception, "an error occured while download data");
            return Option.None<MemoryStream>();
        }
        finally
        {
            sw.Stop();
            Logger.Information("download resource {Url} as stream in {Elapsed:000} ms", url,
                sw.ElapsedMilliseconds);
            cts.Dispose();
        }
    }
}