﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using HashmapCache.Tos;
using Optional;

namespace HashmapCache.Services;

public static class Utils
{
    private static string Concat(this IEnumerable<string> source) => string.Concat(source);

    public static string ComputeHash(HashAlgorithm sha256, string url) => sha256
        .ComputeHash(Encoding.UTF8.GetBytes(url)).Select(bt => bt.ToString("x2")).Concat();

    public static Task<Option<MemoryStream>> DownloadResource(string url, string userAgent, int timeout) =>
        SourceUpdater.DownloadAsStream(url, userAgent, timeout);

    public static string GetFileName(string url)
    {
        var uri = new Uri(url);
        if (uri.LocalPath[(uri.LocalPath.LastIndexOf("/", StringComparison.Ordinal) + 1)..].Length == 0)
            return uri.LocalPath + "index.html";
        return uri.LocalPath[(uri.LocalPath.LastIndexOf("/", StringComparison.Ordinal) + 1)..];
    }

    public static string GetDomainNameWithPath(string url)
    {
        var uri = new Uri(url);
        var host = uri.DnsSafeHost;
        var path = uri.LocalPath.Replace(GetFileName(url), "");
        return $"{host}{path}";
    }

    public static DateTime GetExpireTime(int ttl) => DateTime.Now.AddSeconds(ttl);

    private static void FillItemReturnResult(this GetItemResponseObject itemResponseObject, MemoryStream content,
        string fileToProcess, int ttl, CacheStates cacheStates)
    {
        itemResponseObject.Content = content.Some();
        itemResponseObject.ContentLength = content.Length;
        itemResponseObject.Expiration = File.GetLastWriteTime(fileToProcess).AddSeconds(ttl);
        itemResponseObject.CacheState = cacheStates;
    }

    public static async Task<GetItemResponseObject> WriteToFile(this MemoryStream stream,
        GetItemResponseObject itemResponseObject, string fileToProcess, int ttl, CacheStates states)
    {
        await using var fs = new FileStream(fileToProcess, FileMode.OpenOrCreate, FileAccess.Write);
        await stream.CopyToAsync(fs);
        stream.Position = 0;
        await fs.FlushAsync();
        fs.Close();
        await fs.DisposeAsync();
        itemResponseObject.FillItemReturnResult(stream, fileToProcess, ttl, states);
        return itemResponseObject;
    }

    public static async Task<GetItemResponseObject> ReadStreamFromFile(
        this GetItemResponseObject itemResponseObject,
        string fileToProcess, int ttl, CacheStates cacheStates)
    {
        var memStream = new MemoryStream();
        await using var fs = File.OpenRead(fileToProcess);
        await fs.CopyToAsync(memStream);
        await fs.FlushAsync();
        fs.Close();
        await fs.DisposeAsync();
        memStream.Position = 0;
        itemResponseObject.FillItemReturnResult(memStream, fileToProcess, ttl, cacheStates);
        return itemResponseObject;
    }
}