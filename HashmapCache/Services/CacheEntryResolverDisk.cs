﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using HashmapCache.Configuration;
using HashmapCache.Logging;
using HashmapCache.Tos;
using Optional;
using Serilog;

namespace HashmapCache.Services;

public class CacheEntryResolverDisk : ICacheEntryResolver
{
    private readonly ILogger _logger;
    private readonly SHA256 _sha256;

    public CacheEntryResolverDisk()
    {
        _logger = LoggerBuilder.GetLogger<CacheEntryResolverDisk>();
        _sha256 = SHA256.Create();
    }


    public Task<GetItemResponseObject> GetItem(string url, int ttl, ConfigurationItems configurationItems)
    {
        var hsh = Utils.ComputeHash(_sha256, url);
        var returnResult = new GetItemResponseObject
        {
            FileName = Utils.GetFileName(url),
            Ttl = ttl,
            Hash = hsh,
            Content = Option.None<MemoryStream>()
        };


        if (!Directory.Exists(configurationItems.DiskStorageFolder))
        {
            _logger.Information("storage directory does not exists {Storage}",
                configurationItems.DiskStorageFolder);
            Directory.CreateDirectory(configurationItems.DiskStorageFolder);
        }

        var storeHash = Utils.ComputeHash(_sha256, Utils.GetDomainNameWithPath(url));

        var intermediatePath = $"{configurationItems.DiskStorageFolder}{storeHash}/";
        if (!Directory.Exists(intermediatePath))
        {
            _logger.Information("storage directory does not exists {Storage}", intermediatePath);
            Directory.CreateDirectory(intermediatePath);
        }

        var fileToProcess = $"{intermediatePath}{returnResult.FileName}";

        _logger.Information("process file {File}", fileToProcess);
        return Task.Run(async () =>
        {
            if (File.Exists(fileToProcess))
            {
                if (File.GetLastWriteTime(fileToProcess).AddSeconds(ttl) < DateTime.Now)
                {
                    return await (await Utils.DownloadResource(url, configurationItems.UserAgent,
                            configurationItems.TimeoutWhenInCache))
                        .Match(
                            async stream =>
                            {
                                _logger.Information("EXP: {Url}", url);
                                return await stream.WriteToFile(returnResult, fileToProcess, ttl, CacheStates.Expired);
                            },
                            async () =>
                            {
                                _logger.Information("EXPFC: {Url}", url);
                                return await returnResult.ReadStreamFromFile(fileToProcess, ttl,
                                    CacheStates.ExpiredFromCache);
                            });
                }

                _logger.Information("HIT: {Url}", url);
                return await returnResult.ReadStreamFromFile(fileToProcess, ttl, CacheStates.Hit);
            }

            _logger.Information("MISS: {Url}", url);
            var result = await Utils.DownloadResource(url, configurationItems.UserAgent,
                configurationItems.TimeoutWhenInCache);
            var exitOpt = result.Match(async stream =>
                    await stream.WriteToFile(returnResult, fileToProcess, ttl, CacheStates.Miss),
                () => Task.Run(() => returnResult));

            return await exitOpt;
        });
    }
}