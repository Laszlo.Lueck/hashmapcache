﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Threading.Tasks;
using HashmapCache.Configuration;
using HashmapCache.Logging;
using HashmapCache.Tos;
using Optional;
using Serilog;

namespace HashmapCache.Services;

public class CacheEntryResolverMemory : ICacheEntryResolver
{
    private readonly CustomMemoryCacheImpl _memoryCache;
    private readonly ILogger _logger;
    private readonly SHA256 _sha256;

    public CacheEntryResolverMemory(CustomMemoryCacheImpl memoryCache)
    {
        _memoryCache = memoryCache;
        _logger = LoggerBuilder.GetLogger<CacheEntryResolverMemory>();
        _sha256 = SHA256.Create();
    }

    private Option<CacheObject> GetCacheObject(string hashValue)
    {
        return _memoryCache.TryGetValue(hashValue, out CacheObject cacheObject)
            ? cacheObject.Some()
            : Option.None<CacheObject>();
    }

    private static void AddToCache(CustomMemoryCacheImpl memoryCacheImpl, MemoryStream stream, DateTime expireTime,
        long contentLength, string hash)
    {
        var co = new CacheObject
        {
            Content = stream.ToArray(),
            ContentLength = contentLength,
            ExpireTime = expireTime
        };
        memoryCacheImpl.AddOrUpdate(hash, co);
    }

    private static void ProcessData(MemoryStream stream, CustomMemoryCacheImpl memoryCacheImpl,
        string hash, int ttl, ref GetItemResponseObject responseObject)
    {
        var len = stream.ToArray().Length;
        var expireTime = Utils.GetExpireTime(ttl);
        AddToCache(memoryCacheImpl, stream, expireTime, len, hash);
        responseObject.Content = stream.Some();
        responseObject.Expiration = expireTime;
        responseObject.ContentLength = len;
    }

    private static GetItemResponseObject FillResponseObject(CacheStates cacheState, CacheObject cacheObject,
        GetItemResponseObject responseObject)
    {
        responseObject.CacheState = cacheState;
        responseObject.ContentLength = cacheObject.ContentLength;
        responseObject.Content = new MemoryStream(cacheObject.Content).Some();
        responseObject.Expiration = cacheObject.ExpireTime;
        return responseObject;
    }

    public Task<GetItemResponseObject> GetItem(string url, int ttl, ConfigurationItems configurationItems)
    {
        var hsh = Utils.ComputeHash(_sha256, url);
        var returnResult = new GetItemResponseObject
        {
            FileName = Utils.GetFileName(url),
            Ttl = ttl,
            Hash = hsh,
            Content = Option.None<MemoryStream>()
        };

        var ret = GetCacheObject(hsh).Match(
            async cacheObject =>
            {
                if (cacheObject.ExpireTime > DateTime.Now)
                {
                    _logger.Information("HIT: {Url}", url);
                    return FillResponseObject(CacheStates.Hit, cacheObject, returnResult);
                }

                returnResult.CacheState = CacheStates.Expired;
                _logger.Information("EXP: {Url}", url);

                (await Utils.DownloadResource(url, configurationItems.UserAgent,
                        configurationItems.TimeoutWhenInCache))
                    .Match(
                        stream => ProcessData(stream, _memoryCache, hsh, ttl, ref returnResult),
                        () =>
                        {
                            _logger.Warning("No content from request (possible timeout), deliver cache value");
                            returnResult = FillResponseObject(CacheStates.ExpiredFromCache, cacheObject,
                                returnResult);
                        });


                return returnResult;
            }, async () =>
            {
                _logger.Information("MISS: {Url}", url);
                returnResult.CacheState = CacheStates.Miss;

                (await Utils.DownloadResource(url, configurationItems.UserAgent,
                        configurationItems.TimeoutWhenNotInCache))
                    .MatchSome(stream => ProcessData(stream, _memoryCache, hsh, ttl, ref returnResult));

                return returnResult;
            });

        return ret;
    }
}