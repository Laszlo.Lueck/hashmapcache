﻿namespace HashmapCache.Services;

public enum CacheStates
{
    Hit,
    Miss,
    Expired,
    ExpiredFromCache
}