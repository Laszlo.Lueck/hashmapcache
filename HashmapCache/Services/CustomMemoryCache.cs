﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using HashmapCache.Logging;
using Serilog;

namespace HashmapCache.Services;

public abstract class CustomMemoryCache<TKey, TValue> : IDisposable
{
    internal ConcurrentDictionary<TKey, TValue> MemoryCollection { get; }
    private readonly ILogger _logger;
        
    public int GetCurrentSize => MemoryCollection.Count;

    protected virtual void Dispose(bool disposing)
    {
        _logger.Information("remove all entries from cache while dispose");
        if (disposing) 
        {
            MemoryCollection.Clear();
        }
    }

    protected CustomMemoryCache()
    {
        _logger = LoggerBuilder.GetLogger<CustomMemoryCache<TKey, TValue>>();
        _logger.Information("build the cache object");
        MemoryCollection = new ConcurrentDictionary<TKey, TValue>();
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    public TValue AddOrUpdate(TKey key, TValue value)
    {
        _logger.Information("add new entry to cache {Key}", key);
        return MemoryCollection.AddOrUpdate(key, value, (_, _) => value);
    }

    public bool TryGetValue(TKey key, out TValue value)
    {
        return MemoryCollection.TryGetValue(key, out value);
    }
        
}