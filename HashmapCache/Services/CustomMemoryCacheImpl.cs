﻿using System;
using System.Linq;
using HashmapCache.Tos;

namespace HashmapCache.Services;

public class CustomMemoryCacheImpl : CustomMemoryCache<string, CacheObject>
{
    public int GetExpired => MemoryCollection.Count(kv => kv.Value.ExpireTime < DateTime.Now);
    public int GetValid => MemoryCollection.Count(kv => kv.Value.ExpireTime >= DateTime.Now);
    public long CurrentSize => MemoryCollection.Sum(kv => kv.Value.ContentLength);
}