﻿using System;
using System.IO;

namespace HashmapCache.Tos;

public class CacheObject
{
    public byte[] Content { get; set; }
    public DateTime ExpireTime { get; set; }
    public long ContentLength { get; set; }
}