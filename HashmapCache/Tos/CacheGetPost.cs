﻿using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace HashmapCache.Tos;

public class CacheGetPost
{
    [Required]
    [JsonPropertyName("origin")]
    public string Origin { get; set; }

    [JsonPropertyName("maxTtl")]
    public int MaxTtl { get; set; }
}