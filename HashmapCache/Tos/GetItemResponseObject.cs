﻿using System;
using System.IO;
using HashmapCache.Services;
using Optional;

namespace HashmapCache.Tos;

public class GetItemResponseObject
{
    public CacheStates CacheState { get; set; }
    public DateTime Expiration { get; set; }
    public string FileName { get; set; }
    public Option<MemoryStream> Content { get; set; }
    public string Hash { get; set; }
    public int Ttl { get; set; }
    public long ContentLength { get; set; }
}