﻿namespace HashmapCache.Tos;

public class MemoryCacheStates
{
    public int CurrentSize { get; set; }
    public int ExpiredSize { get; set; }
    public int ValidSize { get; set; }

    public int CacheTtl { get; set; }

    public int TimeoutOnMiss { get; set; }

    public int TimeoutOnExpired { get; set; }

    public long CacheSize { get; set; }
}