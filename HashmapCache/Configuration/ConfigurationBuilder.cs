﻿using System;
using Optional;
using Optional.Linq;

namespace HashmapCache.Configuration;

public sealed record ConfigurationItems(string UserAgent, int TimeoutWhenInCache, int TimeoutWhenNotInCache, int DefaultTtl, string DiskStorageFolder, bool StoreToDisk);

public enum EnvEntries
{
    UserAgent,
    TimeoutWhenInCache,
    TimeoutWhenNotInCache,
    DefaultTtl,
    DiskStorageFolder,
    StoreToDisk
}

public class ConfigurationBuilder
{
    public Option<ConfigurationItems> GetConfiguration()
    {
        Console.WriteLine("Try to read the configuration items from env vars");
        return
            from userAgent in ConfigurationFactory.ReadEnvironmentVariableString(EnvEntries.UserAgent)
            from timeoutWhenInCache in ConfigurationFactory.ReadEnvironmentVariableInt(EnvEntries.TimeoutWhenInCache)
            from timeoutWhenNotInCache in ConfigurationFactory.ReadEnvironmentVariableInt(EnvEntries.TimeoutWhenNotInCache)
            from defaultTtl in ConfigurationFactory.ReadEnvironmentVariableInt(EnvEntries.DefaultTtl)
            from diskStorageFolder in ConfigurationFactory.ReadEnvironmentVariableString(EnvEntries.DiskStorageFolder)
            from storeToDisk in ConfigurationFactory.ReadEnvironmentVariableBool(EnvEntries.StoreToDisk)
            select new ConfigurationItems(userAgent, timeoutWhenInCache, timeoutWhenNotInCache, defaultTtl, diskStorageFolder, storeToDisk);
    }
}