using System;
using HashmapCache.Logging;
using Optional;
using Serilog;

namespace HashmapCache.Configuration;

public static class ConfigurationFactory
{
    public static Option<string> ReadEnvironmentVariableString(EnvEntries value,
        bool returnEmptyStringIfNoValue = false)
    {
        //Put some sugar here to tell why the container stops.
        return Environment.GetEnvironmentVariable(value.ToString()).SomeNotNull().Match(
            Option.Some,
            () =>
            {
                if (returnEmptyStringIfNoValue)
                    return Option.Some(string.Empty);
                Console.WriteLine($"No entry found for environment variable {value}");
                return Option.None<string>();
            }
        );
    }

    public static Option<int> ReadEnvironmentVariableInt(EnvEntries value)
    {
        return Environment.GetEnvironmentVariable(value.ToString()).SomeNotNull().Match(
            variable => int.TryParse(variable, out var intVariable)
                ? Option.Some(intVariable)
                : LogAndReturnNone<int>(value.ToString(), variable),
            () =>
            {
                Console.WriteLine($"No entry found for environment variable {value}");
                return Option.None<int>();
            }
        );
    }

    private static Option<T> LogAndReturnNone<T>(string envName, string value)
    {
        Console.WriteLine($"Cannot convert value {value} for env variable {envName}");
        return Option.None<T>();
    }

    public static Option<bool> ReadEnvironmentVariableBool(EnvEntries value)
    {
        return Environment.GetEnvironmentVariable(value.ToString()).SomeNotNull().Match(
            variable => bool.TryParse(variable, out var boolVariable)
                ? Option.Some(boolVariable)
                : LogAndReturnNone<bool>(value.ToString(), variable),
            () =>
            {
                Console.WriteLine($"No entry found for environment variable {value}");
                return Option.None<bool>();
            }
        );
    }
}