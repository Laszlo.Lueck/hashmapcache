﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using HashmapCache.Configuration;
using HashmapCache.Services;
using HashmapCache.Tos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MimeTypes;

namespace HashmapCache.Controllers;

[ApiController]
[Route("cache")]
public class CacheController : ControllerBase
{
    private readonly CacheEntryResolverMemory _cacheEntryResolverMemory;
    private readonly CacheEntryResolverDisk _cacheEntryResolverDisk;
    private readonly ConfigurationItems _configurationItems;
    private readonly CustomMemoryCacheImpl _memoryCache;


    public CacheController(CustomMemoryCacheImpl memoryCacheImpl, ConfigurationItems configurationItems)
    {
        _memoryCache = memoryCacheImpl;
        _cacheEntryResolverMemory = new CacheEntryResolverMemory(_memoryCache);
        _cacheEntryResolverDisk = new CacheEntryResolverDisk();
        _configurationItems = configurationItems;
    }

    [Route("status")]
    [HttpGet]
    public async Task<IActionResult> GetStatus()
    {
        return await Task.Run(() =>
        {
            var ret = new MemoryCacheStates
            {
                CurrentSize = _memoryCache.GetCurrentSize,
                ExpiredSize = _memoryCache.GetExpired,
                ValidSize = _memoryCache.GetValid,
                TimeoutOnMiss = _configurationItems.TimeoutWhenNotInCache,
                TimeoutOnExpired = _configurationItems.TimeoutWhenInCache,
                CacheTtl = _configurationItems.DefaultTtl,
                CacheSize = _memoryCache.CurrentSize
            };
            return new JsonResult(ret);
        });
    }

    [Route("post")]
    [HttpPost]
    public async Task<IActionResult> PostObject(CacheGetPost request)
    {
        return await GetObject(request.Origin, request.MaxTtl);
    }

    [Route("get")]
    [HttpGet]
    public async Task<IActionResult> GetObject([BindRequired]string origin, int maxTtl)
    {
        var sw = Stopwatch.StartNew();
        var nMaxTtl = maxTtl == 0 ? _configurationItems.DefaultTtl : maxTtl;

        ICacheEntryResolver resolver = _configurationItems.StoreToDisk
            ? _cacheEntryResolverDisk
            : _cacheEntryResolverMemory;
            
        var resultOpt = await resolver.GetItem(origin, nMaxTtl, _configurationItems);
        sw.Stop();

        return resultOpt.Content.Match<IActionResult>(
            value =>
            {
                var mt = MimeTypeMap.GetMimeType(resultOpt.FileName);
                Response.Headers.Add("cache-expiration", resultOpt.Expiration.ToUniversalTime().ToString("R"));
                Response.Headers.Add("cache-state", resultOpt.CacheState.ToString());
                Response.Headers.Add("cache-ttl", resultOpt.Ttl.ToString());
                Response.Headers.Add("cache-hash", resultOpt.Hash);
                Response.Headers.Add("content-length", resultOpt.ContentLength.ToString());
                Response.Headers.Add("content-type", mt);
                Response.Headers.Add("load-time-ms", sw.ElapsedMilliseconds.ToString());
                Response.Headers.Add("Content-Disposition", $"attachment; filename=\"{resultOpt.FileName}\"");
                Response.Headers.Add("Access-Control-Allow-Origin", "*");
                Response.Headers.Add("Access-Control-Allow-Methods", "GET, POST");
                Response.Headers.Add("Access-Control-Allow-Headers","Content-Type");
                return new ObjectResult(value);
            }, () => new NotFoundResult());
    }
}