FROM laszlo/containerruntimeglobal_x64_dotnet_build:23.04_8.0.0 as build-env
WORKDIR /app
COPY ./HashmapCache ./

RUN dotnet restore
RUN dotnet build --no-restore
RUN dotnet publish --no-restore -c Release -o out

FROM laszlo/containerruntimeglobal_x64_dotnet_runtime:23.04_8.0.0
WORKDIR /app
COPY --from=build-env /app/out .
ENV ASPNETCORE_URLS="http://+:4000"
ENTRYPOINT ["dotnet", "HashmapCache.dll"]
