    # HashmapCache

## Introduction
Hashmapcache represents a superfast webcache that load, store and deliver content 
from slower webpages and deliver that stored content from cache.

There are 2 modes available:
1. It stores and read the cached entries completely to / from memory
2. It stores and read the cached entries completely to / from disk.

The 2nd solution is recommended, when the container / storage runs on ssd. Thats fast enough
to serve the cached entries from disk (e.g. 1-3 ms more than serving directly from memory).

It runs as a docker-container. 
The border of stored content is per default 4 GB, when it runs on memory. The border, when
the application stores to disk, depends on your disk-size.

## Installation
The docker-container is stored in a private docker registry with anonymous pull rights!
Feel free to use them!

Here is an example docker-compose file that shows the configuration on a production system.

```
version: "3.7"

services:
  hashmapcache:
    container_name: hashmapcache
    image: nexus.gretzki.ddns.net:10501/hashmapcache:latest
    environment:
      - "UserAgent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36"
      - "ASPNETCORE_ENVIRONMENT=Production"
      - "TimeoutWhenInCache=250"
      - "TimeoutWhenNotInCache=1000"
      - "DefaultTtl=3600"
      - "DiskStorageFolder=./storage/"
      - "StoreToDisk=true"
    networks:
      default:
        ipv4_address: 192.168.19.44
    volumes:
      - /root/projekte/hashmapcache/storage:/app/storage
    expose:
      - "4000"
    ports:
      - "4000:4000"
    logging:
      driver: "json-file"
      options:
        max-size: "10k"
        max-file: "20"
    restart: always
networks:
  default:
    external:
      name: static-net
...
```

The environment parameters are described as follows:
- UserAgent --> The UserAgent is used for the external calls, it simulates a real browser in a (most actual) version.
- ASPNETCORE_ENVIRONMENT --> Internal usage for less log messages and faster speed
- TimeoutWhenInCache --> time in ms, if a (probably slow) webrequest to an external page ist made. When there is after 250ms no response, the cached variable is delivered.
- TimeoutWhenNotInCache --> time in ms, if a (probably slow) webrequest to an external page is made. When there is after 1000ms no response, a 404 would be delivered.
- DefaultTtl --> time in s for a default cache time
- DiskStorageFolder --> storage directory in the container, must match with the settings in the volumes-section.
- StoreToDisk --> when true, the cached content would be stored to disk instead of memory.

The cache works definitely with almost all content-types.

Images, Textfiles, html, xml, json etc.

If you try to cache a whole webpage (html) please be aware, that the app is no proxy!

If you receive the html you can get a lot of Cors or other errors, because the origin domain is not the same as the called one. 

Here are 2 sample requests.
This first use http-GET to get an image from an other webpage.
```
curl -sS -D - https://fast.slowcdn.de/cache/get?origin=https://images.pexels.com/photos/9519451/pexels-photo-9519451.jpeg&maxTtl=10
```


origin --> defines the url where the content would be loaded initialy
maxTtl --> defines the max time to live for the content in cache. If the ttl the content invalidates and the app requests the content new.

If there is no maxTtl-value is provided, the DefaultTtl value from the environment variable of the container would be used.

The second request use http-POST. It is useful when the url from which you receive content contains query-parameter or else.
As above, the body contains two parameters, mandatory origin and optional maxTtl.
To test it out, here is a sample POST-request with curl against a running machine:
```
curl -X POST https://fast.slowcdn.de/cache/post
-H 'Content-Type: application/json'
-d '{"origin":"https://images.unsplash.com/photo-1634195700477-dc1a8403886f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=2072&q=80","maxTtl":"10"}'
```



The cache is tested with jmeter agains one container on a medium fast machine in an internal gigabit network.

100 concurrent user requested 10000 times the app.

No response goes over 35ms, fast enough i think!
